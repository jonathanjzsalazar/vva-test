import React from "react";
import { Switch, Route } from "react-router-dom";

import Sidebar from "./components/sideBar/Sidebar";
import Characters from "./components/characters/Characters";
import Episodes from "./components/episodes/Episodes";
import NotFound from "./components/notFound/NotFound";

import { CHARACTERS, EPISODES } from "./config/routes/Paths";

function App() {
  return (
    <div className='App'>
      <Sidebar />
      <Switch>
        <Route path={CHARACTERS} exact>
          <Characters />
        </Route>
        <Route path={EPISODES} exact>
          <Episodes />
        </Route>
        <Route path='*'>
          <NotFound />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
