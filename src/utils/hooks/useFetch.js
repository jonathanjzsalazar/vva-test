import { useState, useEffect } from "react";

const useFetch = (url) => {
  const [fetchState, setFetchState] = useState({
    isSuccess: false,
    data: null,
  });

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(url);
      const jsonResponse = await response.json();
      return setFetchState({
        isSuccess: response.ok,
        data: jsonResponse,
      });
    };
    fetchData();
  }, [url]);

  return fetchState;
};

export default useFetch;
