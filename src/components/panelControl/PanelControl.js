import React from "react";
import Controls from "./controls/Controls";

import "./PanelControl.css";
import SearchBar from "./searchBar/SearchBar";

const PanelControl = (props) => {

  const searchResultsHandler = (searchResult) => {
    props.onSearchResults(searchResult);
  };
  const selectSortValueHandler = (sortValue) => {
    props.onSelectSortValue(sortValue);
  };
  const changeGridDisplayStyleHandler = () => {
    props.onSelectDisplayStyle("grid");
  };
  const changeListDisplayStyleHandler = () => {
    props.onSelectDisplayStyle("list");
  };

  return (
    <div className='panel-control-main'>
      <SearchBar listOfCharacters={props.charactersList} onSearchResults={searchResultsHandler}/>
      <Controls
        selectedSort={props.selectedSort}
        onSelectSortValue={selectSortValueHandler}
        onChangeGridDisplayStyle={changeGridDisplayStyleHandler}
        onChangeListDisplayStyle={changeListDisplayStyleHandler}
      />
    </div>
  );
};

export default PanelControl;
