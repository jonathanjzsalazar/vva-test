import React, { useState } from "react";

import { ReactComponent as Search } from "../../../images/searchBar/search.svg";

import "./SearchBar.css";

const SearchBar = (props) => {
  const [searchResults, setSearchResults] = useState("");

  const filterInfoHandler = (event) => {
    let stringToFilter = event.target.value.toLowerCase();
    if (stringToFilter.length === 0) {
      props.onSearchResults(null);
    } else if (stringToFilter.length !== 0) {
      let result = props.listOfCharacters.filter((character) =>
        character.name.toLowerCase().includes(stringToFilter)
      );
      setSearchResults(() => result);
      props.onSearchResults(searchResults);
    }
  };

  return (
    <div className='search-bar-main'>
      <input
        type='text'
        placeholder='Search a character'
        className='search-bar-input'
        onChange={filterInfoHandler}
      />
      <Search id='search' />
    </div>
  );
};

export default SearchBar;
