import React, { useState } from "react";
import { NavLink } from "react-router-dom";

import { ReactComponent as List } from "../../../../images/panelControls/list.svg";
import { ReactComponent as Grid } from "../../../../images/panelControls/grid.svg";

import './DisplayStyle.css';

const DisplayStyle = (props) => {
  const [active, setActive] = useState("list");

  return (
    <div className='display-style-main'>
      <NavLink
        to='#'
        activeClassName={active === "list" ? "active-list" : "noActive"}
        onClick={() => setActive(() => "list")}
      >
        <List
          onClick={props.onChangeListDisplayStyle}
        />
      </NavLink>
      <NavLink
        to='#'
        activeClassName={active === "grid" ? "active-grid" : "noActive"}
        onClick={() => setActive(() => "grid")}
      >
        <Grid
          onClick={props.onChangeGridDisplayStyle}
        />
      </NavLink>
    </div>
  );
};

export default DisplayStyle;