import React from "react";

import DisplayStyle from "./displayStyle/DisplayStyle";

import "./Controls.css";

const Controls = (props) => {

  const selectedSortHandler = (event) => {
    const selectedSort = event.target.value;
    props.onSelectSortValue(selectedSort);
  };

  return (
    <div className='controls-main'>
      <div className='controls-sort'>
        <label htmlFor='sort'>Sort by</label>
        <select
          value={props.selectedSort}
          onChange={selectedSortHandler}
          id='sort'
        >
          <option value='old-new'>Oldest - Newest</option>
          <option value='new-old'>Newest - Oldest</option>
        </select>
      </div>
      <DisplayStyle
        onChangeListDisplayStyle={props.onChangeListDisplayStyle}
        onChangeGridDisplayStyle={props.onChangeGridDisplayStyle}
      />
    </div>
  );
};

export default Controls;
