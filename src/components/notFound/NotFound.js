import React from 'react';

import notFoundImg from '../../images/notFound/not-found.png';

import './NotFound.css';

const NotFound = () => {
  return (
    <div className='not-found-main'>
      <img src={notFoundImg} alt='sorry not sorry' id='not-found-image'/>
      <h2 id='not-found-message'>"I'm sorry, &lt;<i>user</i>&gt;. Your opinion means very little to me."</h2>
      <p>Page not found</p>
    </div>
  );
};

export default NotFound;