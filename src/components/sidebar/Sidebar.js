import React from "react";
import { NavLink } from 'react-router-dom';

import { CHARACTERS, EPISODES } from "../../config/routes/Paths";

import logo from "../../images/sideBar/logo.png";
import charactersLogo from "../../images/sideBar/characters-logo.png";
import { ReactComponent as Episodes } from "../../images/sideBar/Episodes.svg";

import "./Sidebar.css";

const Sidebar = () => {
  return (
    <div className='sidebar'>
      <div className='sidebar-items-container'>
        <img src={logo} alt='logo' className='logo' />
        <NavLink to={CHARACTERS} className='sidebar-links' exact>
          <img src={charactersLogo} alt='characters-link' className='characters-logo' />
        </NavLink>
        <NavLink to={EPISODES} className='sidebar-links' exact>
          <Episodes className='episodes-logo'/>
        </NavLink>
      </div>
    </div>
  );
};

export default Sidebar;


