import React, { useState } from "react";

import useFetch from "../../utils/hooks/useFetch";
import { GET_CHARACTERS } from "../../config/routes/ApiAccess";

import PanelControl from "../panelControl/PanelControl";
import Character from "./character/Character";

import rickAndMortyLogo from "../../images/Rick-and-Morty-logo.png";

import "./Characters.css";

const Characters = () => {
  const [searchResults, setSearchResults] = useState([]);
  const [selectedSort, setSelectedSort] = useState("old-new");
  const [displayStyle, setDisplayStyle] = useState("list");

  const { isSuccess, data } = useFetch(GET_CHARACTERS);

  let charactersList = [];
  if (data) {
    charactersList = data.results.map((character) => {
      return {
        created: character.created,
        ...character,
      };
    });
  }

  const searchResultsHandler = (search) => {
    if (search === null) {
      setSearchResults(() => data);
    } else {
      setSearchResults(() => search);
    }
  };
  
  const sortData = (selectedSort, source) => {
    let sortedList;
    if (selectedSort === 'new-old') {
      sortedList = source.sort();
    } else if (selectedSort === 'old-new') {
      sortedList = source.sort().reverse();
    }
    return sortedList;
  }

  const selectSortValueHandler = (sortValue) => {
    setSelectedSort(() => sortValue);
    let sortedList = sortData(selectedSort, charactersList);
    setSearchResults(() => sortedList);
  };
  const selectDisplayStyleHandler = (style) => {
    setDisplayStyle(() => style);
  };

  let mostrarSearchResult;
  if (searchResults.length > 0) {
    mostrarSearchResult = searchResults.map((character) => {
      return (
        <Character
          key={character.id}
          displayStyle={displayStyle}
          name={character.name}
          status={character.status}
          species={character.species}
          lastKnownLocation={character.location.name}
          numOfEpisodes={character.episode.length}
          image={character.image}
          created={character.created}
        />
      );
    });
  }

  let allDataFromAPI;
  if (isSuccess) {
    if (selectedSort)
      allDataFromAPI = data.results.map((character) => {
        return (
          <Character
            key={character.id}
            displayStyle={displayStyle}
            name={character.name}
            status={character.status}
            species={character.species}
            lastKnownLocation={character.location.name}
            numOfEpisodes={character.episode.length}
            image={character.image}
            created={character.created}
          />
        );
      });
  }

  return (
    <div className='characters-main'>
      <img
        src={rickAndMortyLogo}
        alt='Rick and Morty'
        className='rick-and-morty-logo'
      />
      <PanelControl
        charactersList={charactersList}
        onSearchResults={searchResultsHandler}
        selectedSort={selectedSort}
        onSelectSortValue={selectSortValueHandler}
        onSelectDisplayStyle={selectDisplayStyleHandler}
      />
      <hr id='division-line' />
      <div className={`characters-display characters-display-${displayStyle}`}>
        {!isSuccess && (
          <div className='characters-display'>
            <h2 id='loading'>LOADING...</h2>
          </div>
        )}
        {searchResults.length > 0 ? mostrarSearchResult : allDataFromAPI}
      </div>
    </div>
  );
};

export default Characters;
