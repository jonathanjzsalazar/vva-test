import React from "react";

import CharacterImage from "./characterImage/CharacterImage";
import CharacterInfo from "./characterInfo/CharacterInfo";

import "./Character.css";

const Character = (props) => {
  return (
    <div className={`character-display character-display-${props.displayStyle}`}>
      <CharacterImage
        displayStyle={props.displayStyle}
        image={props.image}
        name={props.name}
      />
      <CharacterInfo
        displayStyle={props.displayStyle}
        name={props.name}
        lastKnownLocation={props.lastKnownLocation}
        status={props.status}
        species={props.species}
        image={props.image}
        numOfEpisodes={props.numOfEpisodes}
        created={props.created}
      />
    </div>
  );
};

export default Character;
