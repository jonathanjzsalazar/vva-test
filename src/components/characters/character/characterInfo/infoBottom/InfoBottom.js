import React from "react";

import { ReactComponent as Ellipse } from "../../../../../images/characters/ellipse.svg";
import { ReactComponent as Images } from "../../../../../images/characters/images.svg";
import { ReactComponent as Episode } from "../../../../../images/characters/episode.svg";
import { ReactComponent as Created } from "../../../../../images/characters/created.svg";

import './InfoBottom.css';

const InfoBottom = (props) => {
  return (
    <div className={`information-bottom information-bottom-${props.displayStyle}`}>
      <div className={`information-bottom-fields information-bottom-fields-${props.displayStyle}`}>
        <Ellipse className={`icon ellipse ${props.status}`} />
        <p id='status-species'>{props.status} - {props.species}</p>
      </div>
      <div className={`information-bottom-fields information-bottom-fields-${props.displayStyle}`}>
        <Images className={`icon images`} />
        <p id='images'>{props.image.split(" ").length} images</p>
      </div>
      <div className={`information-bottom-fields information-bottom-fields-${props.displayStyle}`}>
        <Episode className={`icon episode`}/>
        <p id='num-episodes'>{props.numOfEpisodes} Episodes</p>
      </div>
      <div className={`information-bottom-fields information-bottom-fields-${props.displayStyle}`}>
        <Created className={`icon created`} />
        <p id='date-created'>{props.created.slice(0, 10)}</p>
      </div>
    </div>
  );
};

export default InfoBottom;
