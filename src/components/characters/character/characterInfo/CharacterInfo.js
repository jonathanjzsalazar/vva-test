import React from "react";

import InfoTop from "./infoTop/InfoTop";
import InfoBottom from "./infoBottom/InfoBottom";

import "./CharacterInfo.css";

const CharacterInfo = (props) => {
  return (
    <div className={`information information-${props.displayStyle}`}>
      <InfoTop
        displayStyle={props.displayStyle}
        name={props.name}
        lastKnownLocation={props.lastKnownLocation}
      />
      <InfoBottom
        displayStyle={props.displayStyle}
        status={props.status}
        species={props.species}
        image={props.image}
        numOfEpisodes={props.numOfEpisodes}
        created={props.created}
      />
    </div>
  );
};

export default CharacterInfo;
