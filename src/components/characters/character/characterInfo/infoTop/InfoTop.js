import React from "react";

import './InfoTop.css';

const InfoTop = (props) => {
  return (
    <div className={`information-top information-top-${props.displayStyle}`}>
      <p id='name'>{props.name}</p>
      <p id='last-known-location'>{props.lastKnownLocation}</p>
    </div>
  );
};

export default InfoTop;
