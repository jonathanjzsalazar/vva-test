import React from "react";

import './CharacterImage.css';

const CharacterImage = (props) => {
  return (
    <div className={`image-container-${props.displayStyle}`}>
      <img
        src={props.image}
        alt={props.name}
        className='image'
      />
    </div>
  );
};

export default CharacterImage;
