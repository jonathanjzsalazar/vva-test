import React from 'react';

import { GET_EPISODES } from '../../config/routes/ApiAccess';
import useFetch from '../../utils/hooks/useFetch';

import Episode from './episode/Episode';

import './Episodes.css';

const Episodes = () => {
  const {isSuccess, data} = useFetch(GET_EPISODES);
  return (
    <div className='episodes-main'>
      <h3 id='episodes-main-title'>Episodes</h3>
      <div className='episodes-display'>
        {isSuccess && data.results.map(episode => {
          return (
            <Episode key={episode.id} name={episode.name} airDate={episode.air_date} episode={episode.episode} url={episode.url}/>
          )
        })}
        {!isSuccess && <h1>Loading...</h1>}
      </div>
    </div>
  );
};

export default Episodes;