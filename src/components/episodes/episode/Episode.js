import React from 'react';

import './Episode.css';

const Episode = (props) => {
  return (
    <div className='episode-display'>
      <h3 id='episode'>{props.episode}</h3>
      <p id='air-date'>{props.airDate}</p>
      <p id='name'>{props.name}</p>
      <p id='url'>{props.url}</p>
    </div>
  );
};

export default Episode;